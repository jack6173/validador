package com.prontoconsultas.validador;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView txtResultadoCuiSi = (TextView) findViewById(R.id.txtResultadoCuiSi);
        TextView txtResultadoCuiNo = (TextView) findViewById(R.id.txtResultadoCuiNo);
        txtResultadoCuiSi.setVisibility(View.INVISIBLE);
        txtResultadoCuiNo.setVisibility(View.INVISIBLE);
        TextView txtResultadoNitSi = (TextView) findViewById(R.id.txtResultadoNitSi);
        TextView txtResultadoNitNo = (TextView) findViewById(R.id.txtResultadoNitNo);
        txtResultadoNitSi.setVisibility(View.INVISIBLE);
        txtResultadoNitNo.setVisibility(View.INVISIBLE);
    }

    public void validarCui(View view) {
        EditText txtCui = (EditText) findViewById(R.id.edtCui);
        TextView txtResultadoCuiSi = (TextView) findViewById(R.id.txtResultadoCuiSi);
        TextView txtResultadoCuiNo = (TextView) findViewById(R.id.txtResultadoCuiNo);

        String cui = txtCui.getText().toString();
        if(!cui.equals("")) {
            boolean resultado = CuiCheck.validarCUI(cui);
            if (resultado) {
                txtResultadoCuiSi.setVisibility(View.VISIBLE);
                txtResultadoCuiNo.setVisibility(View.INVISIBLE);
            } else {
                txtResultadoCuiSi.setVisibility(View.INVISIBLE);
                txtResultadoCuiNo.setVisibility(View.VISIBLE);
            }
            // Ocultamos el teclado
            txtCui.onEditorAction(EditorInfo.IME_ACTION_DONE );
        }
    }

    public void validarNit(View view) {
        EditText txtNit = (EditText) findViewById(R.id.edtNit);
        TextView txtResultadoNitSi = (TextView) findViewById(R.id.txtResultadoNitSi);
        TextView txtResultadoNitNo = (TextView) findViewById(R.id.txtResultadoNitNo);

        String nit = txtNit.getText().toString();
        if(!nit.equals("")) {
            boolean resultado = NitCheck.validarNIT(nit);
            if (resultado) {
                txtResultadoNitSi.setVisibility(View.VISIBLE);
                txtResultadoNitNo.setVisibility(View.INVISIBLE);
            } else {
                txtResultadoNitSi.setVisibility(View.INVISIBLE);
                txtResultadoNitNo.setVisibility(View.VISIBLE);
            }
            // Ocultamos el teclado
            txtNit.onEditorAction(EditorInfo.IME_ACTION_DONE );
        }
    }

    public void info(View view) {
        Intent intent = new Intent(this, Informacion.class);
        startActivity(intent);
    }

    public void limpiarTodo(View view) {
        EditText txtCui = (EditText) findViewById(R.id.edtCui);
        EditText txtNit = (EditText) findViewById(R.id.edtNit);
        TextView txtResultadoCuiSi = (TextView) findViewById(R.id.txtResultadoCuiSi);
        TextView txtResultadoCuiNo = (TextView) findViewById(R.id.txtResultadoCuiNo);
        TextView txtResultadoNitSi = (TextView) findViewById(R.id.txtResultadoNitSi);
        TextView txtResultadoNitNo = (TextView) findViewById(R.id.txtResultadoNitNo);
        txtCui.setText("");
        txtNit.setText("");
        txtResultadoCuiSi.setVisibility(View.INVISIBLE);
        txtResultadoCuiNo.setVisibility(View.INVISIBLE);
        txtResultadoNitSi.setVisibility(View.INVISIBLE);
        txtResultadoNitNo.setVisibility(View.INVISIBLE);
    }
}
