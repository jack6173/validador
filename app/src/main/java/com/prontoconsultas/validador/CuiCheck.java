package com.prontoconsultas.validador;

/**
 *
 * @author JC
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CuiCheck {

    // Expresión regular para CUI
    private static final String PATTERN_CUI = "^[0-9]{4}\\s?[0-9]{5}\\s?[0-9]{4}$";

    // Funcion que devuelve un Booleano, verdadero si el CUI es válido y falso si no es válido.
    // Recibe como parámetro de entrada el CUI como string
    public static Boolean validarCUI(String Cui) {
        boolean valido = true;

        // Acá se compara la expresión regular con el string ingresado
        Pattern pattern = Pattern.compile(PATTERN_CUI);
        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(Cui);
        boolean validCui = matcher.matches();

        // Se reemplazan los espacios en blanco en la cadena (si tiene)
        Cui = Cui.replace(" ", "");

        //Si la cadena cumple con la expresion regular debemos de verificar que sea un CUI válido
        if (validCui == true) {

            // Extraemos el numero del DPI
            String no = Cui.substring(0, 8);
            // Extraemos el numero de Departamento
            int depto = Integer.parseInt(Cui.substring(9, 11));
            // Extraemos el numero de Municipio
            int muni = Integer.parseInt(Cui.substring(11, 13));

            // Se extra el numero validador
            int ver = Integer.parseInt(Cui.substring(8, 9));

            // Array con la cantidad de municipios que contiene cada departamento.
            int munisPorDepto[] = {
                    /* 01 - Guatemala tiene:      */17 /* municipios. */,
                    /* 02 - El Progreso tiene:    */ 8 /* municipios. */,
                    /* 03 - Sacatepéquez tiene:   */ 16 /* municipios. */,
                    /* 04 - Chimaltenango tiene:  */ 16 /* municipios. */,
                    /* 05 - Escuintla tiene:      */ 13 /* municipios. */,
                    /* 06 - Santa Rosa tiene:     */ 14 /* municipios. */,
                    /* 07 - Sololá tiene:         */ 19 /* municipios. */,
                    /* 08 - Totonicapán tiene:    */ 8 /* municipios. */,
                    /* 09 - Quetzaltenango tiene: */ 24 /* municipios. */,
                    /* 10 - Suchitepéquez tiene:  */ 21 /* municipios. */,
                    /* 11 - Retalhuleu tiene:     */ 9 /* municipios. */,
                    /* 12 - San Marcos tiene:     */ 30 /* municipios. */,
                    /* 13 - Huehuetenango tiene:  */ 32 /* municipios. */,
                    /* 14 - Quiché tiene:         */ 21 /* municipios. */,
                    /* 15 - Baja Verapaz tiene:   */ 8 /* municipios. */,
                    /* 16 - Alta Verapaz tiene:   */ 17 /* municipios. */,
                    /* 17 - Petén tiene:          */ 14 /* municipios. */,
                    /* 18 - Izabal tiene:         */ 5 /* municipios. */,
                    /* 19 - Zacapa tiene:         */ 11 /* municipios. */,
                    /* 20 - Chiquimula tiene:     */ 11 /* municipios. */,
                    /* 21 - Jalapa tiene:         */ 7 /* municipios. */,
                    /* 22 - Jutiapa tiene:        */ 17 /* municipios. */};

            //Verificamos que no se haya ingresado 0 en la posicion de depto o municipio
            if ((muni == 0 || depto == 0) || (muni == 0 && depto == 0)) {
                valido = false;
            } else {
                //Si el numero de depto ingresado en la cadena es mayor 22 es cui invalido
                if (depto > munisPorDepto.length) {
                    valido = false;
                } else {
                    // si depto es menor o igual a 22
                    // se valida que el municipio ingresado en la cadena este dentro del rango del depto
                    if (muni > munisPorDepto[depto - 1]) {
                        valido = false;
                    } else {

                        // si es valido
                        int total = 0;
                        //Se realiza la siguiente Ooperación
                        for (int i = 0; i < no.length(); i++) {
                            total += (Integer.parseInt(no.substring(i, i + 1))) * (i + 2);
                        }

                        // al total de la anterior operación se le saca el mod 11
                        int modulo = total % 11;

                        // Si el mod es igual al numero verificador el cui es valido , sino es invalido
                        if (modulo != ver) {
                            valido = false;
                        }
                    }
                }
            }

        } else {
            valido = false;
        }

        //se retorna el booleano que indica si el cui es válido o no
        return valido;
    }
}
