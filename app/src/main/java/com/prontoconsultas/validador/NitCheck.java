package com.prontoconsultas.validador;

/**
 * @author JC
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NitCheck {

    private static final String PATTERN_NIT = "^[0-9]+(-?[0-9kK])?$";

    public static Boolean validarNIT(String nit) {
        boolean valido = true;

        // Comparamos la expresión regular con el nit ingresado
        Pattern pattern = Pattern.compile(PATTERN_NIT);
        Matcher matcher = pattern.matcher(nit);
        boolean validNit = matcher.matches();

        if (validNit == true) {
            // El formato es correcto
            int i, modulo, resultado, suma = 0, verificador;
            String reverse, verificar;

            //Eliminamos el guión, si existe uno
            nit = nit.replace("-", "");

            // Invertimos la cadena
            reverse = new StringBuffer(nit).reverse().toString();

            // Obtenemos el digito verificador
            String digitoVerificador = reverse.substring(0, 1);
            if (digitoVerificador.equals("K") || digitoVerificador.equals("k")) {
                verificador = 10;
            } else {
                verificador = Integer.parseInt(digitoVerificador);
            }

            // La cadena a verificar
            verificar = reverse.substring(1, reverse.length());

            // multiplicamos cada digito por su posicion (iniciando en 2) y sumamos
            for (i = 0; i < verificar.length(); i++) {
                int c = Character.getNumericValue(verificar.charAt(i));
                suma += c * (i + 2);
            }

            // Dividimos entre 11
            modulo = suma % 11;
            // Tomamos un segundo módulo
            modulo = modulo % 11;

            // Restamos de 11, excepto si el primer m+odulo es cero
            // (daría 11 como resultado, pero el digito verificador es 1- 10)
            resultado = (modulo == 0)? modulo : (11 - modulo);

            // Comparamos el resultado con el dígito verificador
            if (resultado != verificador) {
                valido = false;
            }
        } else {
            valido = false;
        }
        return valido;
    }
}

